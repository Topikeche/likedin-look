export const UPDATE_RECENTVIEW = (idUser) => {
	return {
		type: "UPDATE_RECENTVIEW",
		payload: idUser
	}
}

export const CLEAR_RECENTVIEW = () => {
	return {
		type: CLEAR_RECENTVIEW,
		payload: null
	}
}