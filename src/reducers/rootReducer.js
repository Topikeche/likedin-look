const initState = {
	users:[
		{ 
			id: 1, 
			name: 'Leonard Tupamahu', 
			position: 'Backend Engineer',
			work: 'PT. Sinar Nusantara',
			photo: '1.jpg',
			address: 'Jl Kaliwulung 24, Naglik, Sleman, Yogyakarta',
			connection: 511,
			hasviewed: 37,
			college: 'IT Telekomunikasi'
		},
		{ 
			id: 2, 
			name: 'Risa Saraswati', 
			position: 'Backend Engineer',
			work: 'PT. Sinar Nusantara',
			photo: '2.jpg',
			address: 'Jl Kaliwulung 24, Naglik, Sleman, Yogyakarta',
			connection: 56,
			hasviewed: 46,
			college: 'Universitas Kapan Saja'
		},
		{ 
			id: 3, 
			name: 'Andika Wijaya Kusumah', 
			position: 'Data scientist',
			work: 'PT. Rembulandatang.com',
			photo: '3.jpg',
			address: 'Jl Kaliwulung 24, Naglik, Sleman, Yogyakarta',
			connection: 22,
			hasviewed: 7,
			college: 'UNZ'
		},
		{ 
			id: 4, 
			name: 'Ananda Putri', 
			position: 'Reasearch and Developmnet',
			work: 'PT. Rembulandatang.com',
			photo: '4.jpg',
			address: 'Jl Kaliwulung 24, Naglik, Sleman, Yogyakarta',
			connection: 51,
			hasviewed: 71,
			college: 'Universitas Duabelas April'
		},
		{ 
			id: 5, 
			name: 'Siti Badriah', 
			position: 'Entertainer',
			work: 'MCT Group',
			photo: '5.jpg',
			address: 'Jl Kaliwulung 24, Naglik, Sleman, Yogyakarta',
			connection: 11,
			hasviewed: 1,
			college: 'Instite Bersama'
		}
	],
	userLogin: { 
		id: 2, 
		name: 'Risa Saraswati', 
		position: 'Backend Engineer',
		work: 'PT. Sinar Nusantara',
		photo: '2.jpg',
		address: 'Jl Kaliwulung 24, Naglik, Sleman, Yogyakarta',
		connection: 5,
		hasviewed: 7,
		college: 'Universitas Kapan Saja'
	},
	recentView: []
}

function addRecentView(userId, state) {
	let recent = state.recentView.map(a => a)
	recent.push(userId)
	return {
		...state,
		recentView: recent
	}
}
function clearRecentView(state) {
	return {
		...state,
		recentView: []
	}
}

const rootReducer = (state = initState, action) => {
	if(action.type === 'UPDATE_RECENTVIEW') return addRecentView(action.payload, state)
	if(action.type === 'CLEAR_RECENTVIEW') return clearRecentView(state)
	return state
}

export default rootReducer