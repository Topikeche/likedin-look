import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Homepage from './components/pages/Homepage';
import Profile from './components/pages/Profile';
import Header from './components/partials/Header';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
      <div className="App">
        <Header/>
        <div className="container">
          <main>
            <Switch>
              <Route exact path="/" component={ Homepage } />
              <Route exact path="/profile" component={ Profile } />
              <Route path="/profile/:idUser" component={ Profile } />
            </Switch>
          </main>
        </div>
      </div>
      </BrowserRouter>
    );
  }

}

export default App;
