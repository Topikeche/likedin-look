import React from 'react'

const ListTypeFeed = () => {
	return (
		<div className="box feed-list">
			<div className="header">
				<div className="menu dropdown float-right">
					<a className="btn btn-link btn-sm" href="#!" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i className="material-icons">more_horiz</i>
					</a>						
					<div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
						<a className="dropdown-item" href="#!">Ad Options</a>
					</div>
				</div>
				<h6 className="no-margin">Promote</h6>
			</div>
			<div className="body">
				<div className="item">
					<div>
						<div className="image-wrapper">
							<img src="https://www.shareicon.net/data/128x128/2016/07/05/791215_people_512x512.png" alt="data" />
						</div>
					</div>
					<div><b>Nusantara Indah</b><br/>Our company keeps people safe with positive social outcomes</div>
				</div>
			</div>
		</div>
	)
}

export default ListTypeFeed
