import React from 'react'
import Cover from '../../assets/images/cover.svg'

const MiniProfile = ({ data }) => {
	return (
		<div className="box mini-profile">
			<div className="header">
				<div className="cover-image">
					<img src={ Cover } alt="cover"/>
				</div>
				<div className="profile-image">
					<img src={ require('../../assets/images/' + data.photo) } alt="profile"/>
				</div>
				<div className="identity">
					<h6>{ data.name }</h6>
					<span className="text-grey">{ data.position + ' in ' + data.work }</span>
				</div>
			</div>
			<div className="h-line"></div>
			<div className="connect">
				<div className="list">
					<div>
						<b>
							<span className="text-grey" >Connections</span><br/>
							Grow your network
						</b>
					</div>
					<div className="text-blue"><b>{ data.connection }</b></div>
				</div>
				<div className="list">
					<div><b>Who's viewed your profile</b></div>
					<div className="text-blue"><b>{ data.hasviewed }</b></div>
				</div>
			</div>
			<div className="h-line"></div>
			<div className="additional">
				Access exclusive tools & insights<br/>
				<b>Try Premium Free for 1 Month</b>
			</div>
		</div>
	)
}

export default MiniProfile
