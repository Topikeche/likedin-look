import React from 'react'
import Linkedin from '../../assets/images/linkedin-66.png'

const ListTypePeople = (props) => {
	const additionalClass = (typeof props.class !== 'undefined') ? props.class : ''
	return (
		<div className={"box people-list " + additionalClass}>
			<div className="header">
				<h6 className="no-margin">Add to your feed</h6>
			</div>
			<div className="body">
				<div className="item">
					<div>
						<div className="image-wrapper">
							<img src={ require('../../assets/images/user8-128x128.jpg') } alt="gambar"/>
						</div>
					</div>
					<div>
						<b>Catheryn Vessel</b><br/>
						Reporter at CNN Bussiness
					</div>
				</div>
				<div className="item">
					<div>
						<div className="image-wrapper">
							<img src={ require('../../assets/images/user8-128x128.jpg') } alt="gambar"/>
						</div>
					</div>
					<div>
						<b>Rono Jatmiko</b><br/>
						Reporter at CNN Bussiness
					</div>
				</div>
				<b className="text-blue">View all recommendation</b>
			</div>
			<div className="h-line"></div>
			<div className="footer">
				<img src={ Linkedin } height="14" alt="linkedin"/><small>Linkedin Corporation</small>
			</div>
		</div>
	)
}

export default ListTypePeople
