import React, { Component } from 'react'
import $ from 'jquery'
import avatar from '../assets/images/user8-128x128.jpg'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'


class Search extends Component {

	constructor (props){
		super(props)
		this.state = {
			userInput: '',
			suggestions: []
		}
	}

	matchTerm = (e) => {
		const userInput = e.target.value
		let filteredSuggest = []
		if(userInput.length > 0){
			filteredSuggest = this.props.users.filter(a => a.name.toLowerCase().indexOf(userInput.toLowerCase()) >= 0)
		}
		this.setState({ 
			suggestions: filteredSuggest,
			userInput: userInput 
		})
	}

	suggestSelect(value) {
		this.setState({
			userInput: value
		})
	}

	hancli(e){
		console.log(e)
	}

	show = () => {
		$('#suggestion').show('medium')
	}

	hide = () => {
		$('#suggestion').hide('slow')
	}

	render() {
		let list = [];
		let suggest = this.state.suggestions;
		if(this.state.userInput === ''){
			list.push(<li key="0" className="label"><b>Search for</b></li>)
			list.push(<li key="1"><span className="icon-group"><i className="material-icons text-blue">people</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;People</span></li>)
			list.push(<li key="2"><span className="icon-group"><i className="material-icons text-blue">work</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jobs</span></li>)
			list.push(<li key="4"><span className="icon-group"><i className="material-icons text-blue">chrome_reader_mode</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Content</span></li>)
		} else {
			if(this.state.suggestions.length !== 0){
				for (let i in this.state.suggestions) {
					list.push(
						<li
							key={suggest[i].id}
							onClick={ (x => {
								return event => {
									this.suggestSelect(x);
								};
							})(suggest[i].name) }	
						>
							<Link to={"/profile/" + suggest[i].id } className="full">
								<img src={ avatar } className="photo-list" alt="search profile img" />&nbsp;&nbsp;&nbsp;&nbsp;
								<b>{ suggest[i].name }</b> <span className="text-grey">&nbsp;&nbsp;1st &bull; { suggest[i].position + ' in ' + suggest[i].work }</span> 
							</Link>
						</li>
					);
				}
			} else {
				list.push(<li key="0">Whoops! There's no more suggestion</li>)
			}
		}

		return (
			<div className="search-group">
				<form className="navbar-nav mr-auto text-md-left text-left">
					<div className="input-group">
						<input type="text" value={ this.state.userInput } onChange={ this.matchTerm } onFocus={ this.show } onBlur={ this.hide } className="form-control" placeholder="Search" />
						<div className="input-group-append">
							<button className="btn btn-light" type="button" id="button-addon2"><i className="material-icons">search</i></button>
						</div>
					</div>
					<ul id="suggestion" className="suggestion hidden">
					{list}
					</ul>
				</form>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return{
		users: state.users,
		recentView: state.recentView
	}
}

export default connect(mapStateToProps)(Search)