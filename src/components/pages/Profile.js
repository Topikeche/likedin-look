import React, { Component } from 'react'
import Cover from '../../assets/images/cover.svg'
import { connect } from 'react-redux'
import ListTypeFeed from '../widgets/ListTypeFeed'
import ListTypePeople from '../widgets/ListTypePeople'

class Profile extends Component {

	constructor (props){
		super(props)
		this.state = {
			userData: {}
		}
	}

	componentWillMount = () => {
		let idUser = this.props.match.params.idUser
		if(typeof idUser !== 'undefined'){
			let data = this.props.users.find(a => a.id == idUser)
			this.setState({ userData: data })
		} else{
			this.setState({ userData: this.props.userLogin })
		}
	}
	componentWillReceiveProps = (nextProps) => {
		let idUser = nextProps.match.params.idUser
		let data = this.props.users.find(a => a.id == idUser)
		this.setState({ userData: data })
	}

	render() {
		return (
			<div className="row">
				<div className="col-8">
					<div className="box profile">
						<div className="identity">
							<div className="cover-image-wrap">
								<img src= { Cover } alt="cover"/>
							</div>
							<div className="profile-image-wrap">
								<img src={ require('../../assets/images/'+this.state.userData.photo) } alt="gambarkuh"/>
							</div>'
							<div className="info">
								<div className="row">
									<div className="col-8">
										<h4>{ this.state.userData.name }</h4>
										<h6>{ this.state.userData.position}</h6>
										<p className="text-grey">{ this.state.userData.address }</p>
										<span className="dropdown">
											<button className="btn btn-primary linkedin dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Add Profile Section&nbsp;&nbsp;
											</button>
											<div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<a className="dropdown-item" href="#!">Action</a>
												<a className="dropdown-item" href="#!">Another action</a>
											</div>
										</span>&nbsp;&nbsp;
										<button type="button" className="btn btn-outline-secondary">More...</button>
									</div>
									<div className="col-4">
										<div>{ this.state.userData.position }</div>
										<div>{ this.state.userData.college }</div>
										<div>See contact info</div>
										<div>See connections ({ this.state.userData.connection })</div>
									</div>
								</div>
								<div className="h-line mt-3 mb-3"></div>
								<p>I’m a 21 years old, diligent, hard worker and honest person. I’m be able to work individually and also within a team setting, eager to learn and absorb as much knowledge in the pursuance of my goals. Also, I would like to work with professionals to improve my skills and contribute better.</p>
								<div className="doc-wrapper">
									<img src="https://media.licdn.com/media-proxy/ext?w=214&h=200&f=n&hash=ZA4SIOn4AwoL%2F6Zp7zto7gzIfe4%3D&ora=1%2CaFBCTXdkRmpGL2lvQUFBPQ%2CxAVta5g-0R6plRdUxg4u8KyOtVyk5VNORszfDW2-GGH7vozTfy60KJCKOq39-wpJF39Q1FVuILi3F2yoUsvTdouff5RszcG5VZa4UiE8Jww4gg" alt="dropbox"/>
									<i className="material-icons">link</i>
								</div>
							</div>
						</div>
					</div>
					<div className="box dashboard mt-3">
						<div className="header">
							<h4>Your Dashboard</h4>
							<p>Private to you</p>
						</div>
						<div className="counter">
							<div><span className="number">22</span><br/>Who viewed your profile</div>
							<div><span className="number">2</span><br/>Article views</div>
							<div><span className="number">12</span><br/>Search appearances</div>
						</div>
						<div className="career mt-3">
							<div><i className="material-icons text-grey">work</i></div>
							<div>
								<b>Career Interests</b><br/>
								Lets recruiters know you're open <span className="badge badge-success">On</span><br/>
								<div className="text-grey pt-2">Choose the types of oportunity you'd like to be connected with</div>
							</div>
						</div>
					</div>
				</div>
				<div className="col-4">
					<ListTypeFeed/>
					<div className="v-space"></div>
					<ListTypePeople className="transparent"/>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		userLogin: state.userLogin,
		users: state.users
	}
}

export default connect(mapStateToProps)(Profile)
