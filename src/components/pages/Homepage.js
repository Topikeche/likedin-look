import React, { Component } from 'react'
import { connect } from 'react-redux'
import MiniProfile from '../widgets/MiniProfile';
import ListTypePeople from '../widgets/ListTypePeople';
import PostForm from '../PostForm';

class Homepage extends Component {

	constructor (props){
		super(props)
		this.state = {
			userInput: ''
		}
	}

	render() {
		return (
			<div className="row">
				<div className="col-3">
					<MiniProfile data={ this.props.userLogin } />
				</div>
				<div className="col-6">
					<PostForm/>
				</div>
				<div className="col-3">
					<ListTypePeople/>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return{
		userLogin: state.userLogin
	}
}

export default connect(mapStateToProps)(Homepage)
