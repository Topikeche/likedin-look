import React from 'react'
import { Link } from 'react-router-dom'
import linkedInLogo from '../../assets/images/linkedin-square.png'
import Search from '../Search'
import { connect } from 'react-redux'

const Header = (props) => {
	const userData = props.userLogin
	return (
		<nav className="navbar fixed-top navbar-expand-sm navbar-dark bg-dark-blue">
			<div className="container">
			<Link to="/" className="navbar-brand">
				<img src={ linkedInLogo } width="34" height="34" alt="logo" />
			</Link>
			<div className="navbar-collapse collapse justify-content-between align-items-center w-100" id="collapsingNavbar2">
				<Search/>
				<ul className="nav ml-auto">
					<li className="nav-item">
						<Link to="/" className="nav-link sub-icon">
							<span className="box-icon">
								<i className="material-icons">home</i>
							</span>
							<br/>
							<span className="nav-icon-title">Home</span>
						</Link>
					</li>
					<li className="nav-item">
						<a className="nav-link sub-icon" href="#!">
							<span className="box-icon">
							<i className="material-icons">people</i>
							</span>
							<br/>
							<span className="nav-icon-title">My Network</span>
						</a>
					</li>
					<li className="nav-item">
						<a className="nav-link sub-icon" href="#!">
							<span className="box-icon">
							<i className="material-icons">work</i>
							</span>
							<br/>
							<span className="nav-icon-title">Jobs</span>
						</a>
					</li>
					<li className="nav-item">
						<a className="nav-link sub-icon" href="#!">
							<span className="box-icon">
							<i className="material-icons">forum</i>
							</span>
							<br/>
							<span className="nav-icon-title">Messaging</span>
						</a>
					</li>
					<li className="nav-item">
						<a className="nav-link sub-icon" href="#!">
							<span className="box-icon">
								<i className="material-icons">notifications</i>
								<span className="count">5</span>
							</span>
							<br/>
							<span className="nav-icon-title">Notifications</span>
						</a>
					</li>
					<li className="nav-item dropdown">
						<span className="nav-link sub-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span className="box-icon">
								<img src={ require('../../assets/images/'+userData.photo) } className="photo-header" alt="header profile img" />
							</span>
							<br/>
							<span className="nav-icon-title">Me&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i className="material-icons">arrow_drop_down</i></span>
						</span>
						<div className="dropdown-menu dropdown-menu-right account">
							<div className="header">
								<div>
									<img src={ require('../../assets/images/'+userData.photo) } alt="profile"/>
								</div>
								<div className="pt-2">
									<h6 className="no-margin">{ userData.name }</h6>
									<span className="text-grey">{ userData.position }</span>
								</div>
							</div>
							<Link to="/profile" className="dropdown-item text-blue text-center"><b>View Profile</b></Link>
							<span className="dropdown-caption">ACCOUNT</span>
							<a className="dropdown-item" href="#!">Setting & Privacy</a>
							<a className="dropdown-item" href="#!">Language</a>
							<span className="dropdown-caption">NEED HELP</span>
							<a className="dropdown-item" href="#!">Open Quick Help</a>
							<span className="dropdown-caption">MANAGE</span>
							<a className="dropdown-item" href="#!">Post & Activity</a>
							<a className="dropdown-item" href="#!">Job Postings</a>
							<a className="dropdown-item" href="#!">Sign Out</a>
						</div>
					</li>
					<li className="separator"></li>
					<li className="nav-item dropdown">
						<span className="nav-link sub-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i className="material-icons">apps</i><br/>
							<span className="nav-icon-title">Work&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i className="material-icons">arrow_drop_down</i></span>
						</span>
						<div className="dropdown-menu dropdown-menu-right">
							<a className="dropdown-item" href="#!">Action</a>
							<a className="dropdown-item" href="#!">Another action</a>
						</div>
					</li>
				</ul>
				<span className="justify-content-end"> ?  </span>
			</div>
			</div>
		</nav>
	)
}

const mapStateToProps = (state) => {
	return{
		userLogin: state.userLogin
	}
}

export default connect(mapStateToProps)(Header)
