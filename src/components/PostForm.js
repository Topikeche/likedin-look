import React, { Component } from 'react'

class PostForm extends Component {
	render() {
		return (
			<div className="box">
				<div className="post-add">
					<div className="body">
						<div className="post-box pl-3">
							<span className="icon-text-inline">
								<i className="material-icons">create</i>&nbsp;&nbsp;<b>Start a Post</b>
							</span>
						</div>
						<div className="bor-left icon-only"><i className="material-icons">photo_camera</i></div>
						<div className="bor-left icon-only"><i className="material-icons">videocam</i></div>
						<div className="bor-left icon-only"><i className="material-icons">insert_drive_file</i></div>
					</div>
					<div className="footer">
						<b className="text-blue">Write an article</b> on LinkedIn
					</div>
				</div>
			</div>
		)
	}
}

export default PostForm
